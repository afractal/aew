; a program to filter gridded data in space and time
;
;
;
;
;---------------------------------------------------------------------------
load "diagnostics_cam.ncl"


begin

; level
  plev = 850.
  FlipLon = False
  
  
; long bounds for analysis

  lonW = 39 ; integer
  lonE = 160 ; integer

; lat bounds
  latS = 10.
  latN = 25.
  
  taperPercent = 0.1
  
; list of variables that we need
  variables = (/"V","U","W","Z","T","Q"/)
; what are the filenames called? set the prefix here
  varFile = (/"uv","uv","omega","geo","geo","hum"/)
; the names of variables in the datafiles
  varName = variables + "_GDS4_ISBL"
  

  sTime = 051500 ;050100
  eTime = 101518 ;113018

  spd = 4 ; 4 samples per day for 6 hourly data
  ystart = 2000
  nyears = 1


; set variable to operate on  
  ivar = 0

  
;  dataDir = "~/data100/era/"  ;"/typhoon/era/"

  dataDir = "/typhoon/era/"

  do iy = ystart,ystart+nyears-1
    sDate = iy*1000000 + sTime
    eDate = iy*1000000 + eTime
    allTimes := yyyymmddhh_time(iy,iy,6, "integer")
    TIME     := allTimes({sDate:eDate})
; now define all files 
    fils :=  dataDir + TIME + "." +  varFile(ivar)  + ".grib"
    in   := addfiles (fils, "r")
    ListSetType (in, "join")             ; concatenate or "merge" (default)
    delete(allTimes)
    delete(TIME) 
; now read the data 
    if (FlipLon) then
      dat := lonFlip(in[:]->$varName(ivar)$(:,{plev},{latS:latN},:))
    else
      dat := in[:]->$varName(ivar)$(:,{plev},{latS:latN},:)
    end if
    
    dat2 = dat(:,:,{lonW:lonE})    
    dat := dat2
    printVarSummary(dat)
    
    dat = taper_n(dat,taperPercent,0,0)
    dat = taper_n(dat,taperPercent,0,2)
    
    
    dims = dimsizes(dat)
    nt   = dims(0)
    ny   = dims(1)
    nx   = dims(2)

    ndays = nt/4
    print (ndays)
    
; create the array to hold the transform data

    datx = new( (/2,nx,nt/),      typeof(dat) )
    datW = new( (/ny,nx+1,nt+1/), typeof(dat) )

    printVarSummary(datx)

    do j = 0,ny-1 ; loop over latitudes

; taper over time and long


      
; first the space transform in longitude direction      
      do l = 0,nt-1 ; loop over times
        datx(:,:,l) = cfftf (dat(l,j,:), 0.0, 0)    ; imaginary part set to 0.0
      end do
      datx = datx/nx
; next the transform in time for each longitudinal point      
      do i = 0,nx-1 ; loop over longs
        datx(:,i,:) = cfftf (datx(0,i,:),datx(1,i,:), 0)   
      end do
      datx = datx/nt      
      datW(j,:,:) = resolveWavesHayashi( datx, ndays, 4 )
      
    end do

    printVarSummary(datW)

   
;-------------------------------------------------------------------
; plotting parameters freq and wavenumbers to plot
;-------------------------------------------------------------------



    frqfftwin      = datW&freq
    frqfftwin&freq = datW&freq

    wavep1         = datW&wave
    wavep1&wave    = datW&wave
    
    minfrq4plt =  0.
    maxfrq4plt =  0.8
    minwav4plt = -15.
    maxwav4plt =  15.

    minfrq     = minfrq4plt
    maxfrq     = min((/maxfrq4plt,max(frqfftwin)/))
    
    fillVal    = 1e20           ; miscellaneous
    

   freq       = frqfftwin({freq|minfrq:maxfrq})
   wavenumber = wavep1({wave|minwav4plt:maxwav4plt})
   NWVN       = dimsizes(wavenumber)         ; number of wavenumbers


; be sure to define wavenumber so that it is relative to the full earth circumference
; wavenumber is defined to be an integer. So this is an approximate conversion
   wavenumber = wavenumber*360/(lonE-lonW)
    
    
    power = dim_sum_n_Wrap(datW,0)
    printVarSummary(power)
    power(:,{0.0}) = (/power@_FillValue /)  

    minwav4smth = -27
    maxwav4smth =  27
    
    do wv=minwav4smth,maxwav4smth
      wk_smooth121( power({wv},nt/2+1:nt-1) )
    end do    
    power = log10(power)
    wks  = gsn_open_wks("ncgm","test")
    res = True
    res@gsnCenterString = "Test"

    tmFontHgtF            = 0.015     ; not sure why
    tiFontHgtF            = 0.018
    lbFontHgtF            = 0.015
    txFontHgtF            = 0.013
      
      res@gsnFrame          = False
      res@gsnMaximize       = True
      res@gsnPaperOrientation = "portrait"
      
      res@gsnLeftString     = "Westward"
   res@gsnRightString    = "Eastward"

  ;res@lbBoxMinorExtentF = 0.18
   res@lbLabelFontHeightF= lbFontHgtF
   res@lbOrientation     = "vertical"

   res@cnFillOn          = True
   res@cnLinesOn     = False


   res@tmYLMode          = "Explicit"
   res@tmYLValues        = fspan(minfrq,maxfrq,9)
   res@tmYLLabels        = fspan(minfrq,maxfrq,9)
   res@tmYLMinorValues   = fspan(minfrq,maxfrq,17)

   res@tmYLLabelFontHeightF = tmFontHgtF
   res@tmXBLabelFontHeightF = tmFontHgtF

   res@tiXAxisString     = "Zonal Wave Number"
   res@tiXAxisFontHeightF= tiFontHgtF

   res@tiYAxisString     = "Frequency (cpd)"
   res@tiYAxisFontHeightF= res@tiXAxisFontHeightF




   asym       = power({freq|minfrq:maxfrq},{wave|minwav4plt:maxwav4plt})
   asym!0     = "freq"
   asym&freq  =  freq 
   asym!1     = "wave"
   asym&wave  =  wavenumber
   asym@long_name = "Fig_1: log10(Asymmetric)"

   
    
    plot = gsn_csm_contour(wks,asym,res)
    frame(wks)
    delete(wks)    ; not required 

    

  end do

  
end

