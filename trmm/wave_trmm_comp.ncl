; NCSU Tropical Dynamics
;
; This program will read weather states data and the wave dates from
; James Russell's files and create histograms
;
;
;---------------------------------------------------------------------



begin

  dir = "~/data100/james/"
  
;  Lat      = 10.5  
;  Lon      = -3.
  
;  Lat = 11
;  Lon = 33

;  Lat = 10.5
;  Lon = 15
  
;  Lat =  12
;  Lon = -16
  
  Lat =  14.5
  Lon = -32
  
  
  Lev      = 650
  vrange   = "-1-9"




  fileName = "../../weather_states/IRws_jas_climo_freq.nc"
  inFile   = addfile( fileName, "r" )  
  climoFreq= inFile->freq

  dims = dimsizes(climoFreq)
  ny = dims(1)
  nx = dims(2)        


  Freq = climoFreq
  Freq = 0.0
  
  dir = "~/data100/james/"

  iTimes  = 0
  do year = 1984,2007
    inFile := dir + "wavedates_"  + Lat + Lon + Lev + vrange + "_" + year + ".nc"    
    f = addfile (inFile,"r")
    wavedates := f->wavedates
    waveno    := f->waveno
    wavevwnd  := f->wavevwnd
    dims = dimsizes(wavevwnd)
    wsFile  = "/home/anant/data100/ws/IRregimes_"+year+".nc"
    f = addfile (wsFile,"r")

    timeUnits = f->time@units
    
    
    nt = dims(0)
    do i = 0,nt-1
      iy = wavedates(i)/1000000
      im = (wavedates(i) - iy*1000000)/10000
      id = (wavedates(i) - iy*1000000 - im*10000)/100
      ih =  wavedates(i) - (wavedates(i)/100)*100
      
      print ( wavedates(i) + " " + iy + " " + im + " " + id + " " + ih)
      dateWS = cd_inv_calendar(iy,im,id,ih,0,0,timeUnits,0)

      ; original ws
      ;ws := short2flt(f->2Fregime({dateWS},:,:))

      ; IR ws
      ws := short2flt(f->2Fregime({dateWS},:,:))
; For now delete the fill Value attribute since it is 0
; That will help us use the where construct below.
; Don't Ask! It was a pain to figure this one out   
      delete(ws@_FillValue)
      delete(ws@_FillValue_original)

      do iw = 1,8
        do it = 0,nt-1
          Freq(iw,:,:)= where( (ws.eq.iw),Freq(iw,:,:)+1., Freq(iw,:,:))  
        end do
      end do
      iTimes = iTimes + 1

    end do
  end do
  
  print ( "iTimes = " + iTimes )

   ; now that we have counts at each point, convert to
; percent
    
    do j=0,ny-1
      do i = 0,nx-1
          Freq(:,j,i) = 100.*Freq(:,j,i)/sum(Freq(:,j,i))
      end do
    end do




;************************************************
; create plot
;************************************************
  wks = gsn_open_wks("ncgm","color")             ; send graphics to PNG file
  res                      = True               ; plot mods desired


  res@lbOrientation        = "horizontal"         ; vertical label bars


  gsn_define_colormap(wks,"sunshine_9lev")
  
  
  res@mpFillOn             = False              ; turn off gray continents
  res@mpCenterLonF         = 0.                ; Centers the plot at 180 
  res@mpMinLatF            = -5      ; range to zoom in on
  res@mpMaxLatF            =  30.
  res@mpMinLonF            =  -60.
  res@mpMaxLonF            =   60.
  
  
  res@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
  res@cnMinLevelValF       =  5.              ; set min contour level
  res@cnMaxLevelValF       =  45.                 ; set max contour level
  res@cnLevelSpacingF      =  5.               ; set contour spacing
  
  
  res@cnFillOn         = True               ; color Fill 
 ; res@cnFillMode       = "RasterFill"       ; Raster Mode
  res@cnLinesOn        =  False             ; Turn off contour lines
;================================
; these three resources make the continents look nice. The first two
; make them color, and the later adds continental outlines when
; raster mode is used.
  
  res@cnLineDrawOrder  = "Predraw"          ; Draw lines and filled
  res@cnFillDrawOrder  = "Predraw"          ; areas before map gets set
  

      
  res@txFontHeightF = .5
  res@lbLabelFontHeightF =.022                 ; make labels larger
  res@tiMainFontHeightF  = 0.01                    ; change font heights
  res@gsnDraw             = False           ; don't draw
  res@gsnFrame            = False           ; don't advance frame
  
  txres               = True   
  txres@txFontHeightF = .03             ; Set the font height

   
  plot = new(3,"graphic")

  
  do k=1,8    
    res@tiMainString         = "WS " + k + " " + min(Freq(k,:,:)) + " " + max (Freq(k,:,:))

    res@cnMinLevelValF       =  5.              ; set min contour level
    res@cnMaxLevelValF       =  45.                 ; set max contour level
    res@cnLevelSpacingF      =  5.               ; set contour spacing


 
    res@cnFillOn         = False               ; color Fill                
    res@cnLinesOn        = True             ; Turn off contour lines
;
    
;    gsn_define_colormap(wks,"sunshine_9lev")
    plot(0) = gsn_csm_contour_map(wks,Freq(k,:,:), res)
    plot(1) = gsn_csm_contour_map(wks,climoFreq(k,:,:), res)

    diff = Freq(k,:,:)
    diff = Freq(k,:,:)-climoFreq(k,:,:) 

    res@gsnContourNegLineDashPattern = 1 
    res@cnFillOn         =  True               ; color Fill                
    res@cnLinesOn        =  True             ; Turn off contour lines
;
    res@cnMinLevelValF       =  -25.              ; set min contour level
    res@cnMaxLevelValF       =   25.                 ; set max contour level
    res@cnLevelSpacingF      =    5.               ; set contour spacing
    gsn_define_colormap(wks,"CBR_coldhot")
    plot(2) = gsn_csm_contour_map(wks,diff, res)
    hurri = gsn_add_text(wks,plot(0), "N",Lon,Lat, txres )
    hurri = gsn_add_text(wks,plot(1), "N",Lon,Lat, txres )
    hurri = gsn_add_text(wks,plot(2), "N",Lon,Lat, txres )

    gsn_panel(wks,plot,(/3,1/),False) 
      
  end do
  
  



end

