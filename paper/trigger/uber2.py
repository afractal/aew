#!/usr/bin/env python3
#
# 
#
#
# A. Aiyyer 
# NCSU Tropical Dynamics Group
#--------------------------------------------------------------------------

from subprocess import call

dir = "../../forcing/anomHeat/data/"

ni = 26
nj = 41

Lat0  = -31.25
Lon0  = -50.  

dlat =  2.5
dlon =  2.5

for i in range(1,ni+1):
    Lat = Lat0 + (i-1)*dlat

    for j in range(1,nj+1):
        Lon = Lon0 + (j-1)*dlon
        #print("%f  %f" % (Lat,Lon))
        
        argu2 = "./output_"  + str(Lat) + "_" + str(Lon)
        argu3 = "varbin/u.bin_"+ str(Lat) + "_" + str(Lon)
        argu4 = "varbin/v.bin_"+ str(Lat) + "_" + str(Lon)

        print(argu2)

        call("ln -sf " + argu2 + " fort.13", shell=True)
        call("/home/anant/data50/laptop/work/models/igcm/t31/process/specan")
        call("./variables")        
        call("mv u.bin " + argu3, shell=True)
        call("mv v.bin " + argu4, shell=True)

