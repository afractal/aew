#!/usr/bin/env python3
#
# Code run the igcm multiple times by accessing forcing fields
# created earlies
#
#
# A. Aiyyer 
# NCSU Tropical Dynamics Group
#--------------------------------------------------------------------------

from subprocess import call



dir = "../../forcing/anomHeat/data/"

ni = 26
nj = 41

Lat0  = -31.25
Lon0  = -50.  

dlat =  2.5
dlon =  2.5

for i in range(1,ni+1):
    Lat = Lat0 + (i-1)*dlat

    for j in range(1,nj+1):
        Lon = Lon0 + (j-1)*dlon
        #print("%f  %f" % (Lat,Lon))

        
        argu1 = dir + "deep_convective_"  + str(Lat) + "_" + str(Lon)
        argu2 = "data/output_"  + str(Lat) + "_" + str(Lon)

        
        print(argu1)
        print(argu2)

        call("ln -sf " + argu1 + " fort.15", shell=True)
        call("./prog")

        call('"mv" fort.9 ' + argu2, shell=True)

