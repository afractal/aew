;---------------------------------------------------------------------------
; AEW Diagnostics Package
; NCSU Tropical Dynamics Group
;
;
;
; program to create high pass filtered fields by reading year by year
; currently configured for daily era interim files (one file per day) on
; our server.
; cannot cross over years (needs update to do so). 
;
; Aiyyer 08/2018
;
; 
;---------------------------------------------------------------------------
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"


begin

; choose the range of dates to read
; this should have enough data for padding the filter on either end
    MMDD_start = 0401
    MMDD_end   = 1231 

; choose the range of dates for the filtered output
    MMDDHH_fiter_start  = 061500
    MMDDHH_filter_end   = 101500

;
; set parameters the filter
;
  dt = 1.     ;days per time step
  t1 = 10      ;days  (low frequency cutoff, expressed in time domain)
  t2 = -999.   ;days  (high frequency cutoff, expressed in time domain)
  ihp = 1      ;1=high pass
  fca = dt/t1
  fcb = -999
  sigma   = 1.0                           ;Lanczos sigma
  nWgt    = 61                           ;loose 60 points each end                            
  wgt     = filwgts_lanczos (nWgt, ihp, fca, fcb, sigma )
;
; 
;
  icount = 0


  
  do iy = 1984,1984

    print ( "Year = " + iy )
    sDate = iy*10000 +  MMDD_start
    eDate = iy*10000 +  MMDD_end
    allTimes = yyyymmdd_time(iy,iy, "integer")
    TIME = allTimes({sDate:eDate})

;************************************************************
; for u and v
    fils = "/typhoon/eradaily/" + TIME + ".nc"    

;************************************************************

    in      = addfiles (fils, "r")
    ListSetType (in, "join")             ; concatenate or "merge" (default)

; make changes to variable name here. also change in the output portion (see comment there)
;   dat = in[:]->V_GDS4_ISBL(:,{50:1000},{-20:45},:)     
;   dat = in[:]->U_GDS4_ISBL(:,{50:1000},{-20:45},:)   
;   dat = in[:]->Z_GDS4_ISBL(:,{50:1000},{-20:45},:)  
;   dat = in[:]->W_GDS4_ISBL(:,{50:1000},{-20:45},:)


; V
    dat := in[:]->V(:,{600:925},{-20:45},:)  

    dat!0    = "time" 

; add the hour info to the TIME
    
    dat&time = TIME*100   ; cannot be string
    dat!1    = "lev"     
    dat!2    = "lat"     
    dat!3    = "lon"    
    
    FDAT      =  wgt_runave_n_Wrap ( dat,  wgt, 0, 0 )


; just retain a subset of the data since the ends of the filtered data are missing (due to weights)    
    sDate = iy*1000000 + MMDDHH_fiter_start
    eDate = iy*1000000 + MMDDHH_filter_end
    VHP = FDAT({sDate:eDate},:,:,:) 
    delete(FDAT)
    printVarSummary(VHP)    
   
; get the arrays for lev, lat and lon
    time= VHP&time
    lev = VHP&lev
    lat = VHP&lat
    lon = VHP&lon
;    print ( time + " " + VHP(:,{650.},{10.},{345.} ) )

;
;    
;    printVarSummary(VHP)
    ntim  = dimsizes(time)                 ; get dimension sizes  
    klev  = dimsizes(lev)                                               
    nlat  = dimsizes(lat)  
    nlon  = dimsizes(lon)  
;    
;    
    setfileoption("nc","format","netcdf4") 
    outfile = "V10DayHP" + iy + ".nc"     ; change file name to reflect variable
    system( "rm " + outfile )

    fout    = addfile (outfile, "c" )

;===================================================================
; explicitly declare file definition mode. Improve efficiency.
;===================================================================
    setfileoption(fout,"DefineMode",True)
;===================================================================
; create global attributes of the file
;===================================================================
    fAtt               = True            ; assign file attributes
    fAtt@title         = "NCL Efficient Approach to netCDF Creation"  
    fAtt@source_file   =  fils(0)
    fAtt@Conventions   = "None"   
    fAtt@creation_date = systemfunc ("date")        
    fileattdef( fout, fAtt )            ; copy file attributes  
;===================================================================
; predefine the coordinate variables and their dimensionality
; Note: to get an UNLIMITED record dimension, we set the dimensionality
; to -1 (or the actual size) and set the dimension name to True.
;===================================================================
    dimNames = (/"time", "lev", "lat", "lon"/)  
    dimSizes = (/ ntim,   klev,  nlat,  nlon/) 
    dimUnlim = (/ False , False, False, False/)   

    filedimdef(fout,dimNames,dimSizes,dimUnlim)
    
    filevardef(fout, "time" ,typeof(time),getvardims(time)) 
    filevardef(fout, "lev"  ,typeof(lev), getvardims(lev) )                           
    filevardef(fout, "lat"  ,typeof(lat), getvardims(lat))                          
    filevardef(fout, "lon"  ,typeof(lon), getvardims(lon))   
    
    filevardef(fout, "VHP"    ,typeof(VHP), getvardims(VHP))    
    
;===================================================================
; explicitly exit file definition mode. **NOT REQUIRED**
;===================================================================
    setfileoption(fout,"DefineMode",False)

    fout->VHP    = (/VHP/)    ; change variable name 
    fout->time   = (/time/)     
    fout->lev    = (/lev/)
    fout->lat    = (/lat/)
    fout->lon    = (/lon/) 
    
    delete(allTimes)
    delete(time)
    delete (VHP)
    delete(fils)
  end do
  
  
end



