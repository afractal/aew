!---------------------------------------------------------------
  !
  ! program reads the file from James that has the list of TCs
  ! that originated from AEWs. Then it reads the file with
  ! all TC genesis date/locations (created by genesis_location.ncl)
  ! taken from the ibtracs file. It matches the TCs from the 
  ! two files and outputs the name, date and lat-lon for each 
  ! TC that originated from AEW
  !
  ! reads  (inputs):  aew_sheet.csv, genesis_locations.txt
  ! writes (output):  aew_tc_info.txt
  ! Tropical Dynamics Group
  ! NCSU
  ! A. Aiyyer 2017
  !
  !---------------------------------------------------------------

  implicit none
  !
  ! function borrowed from someone else
  ! 
  INTERFACE 
     Function to_upper (str) Result (string)  
       Character(*), Intent(In) :: str
       Character(LEN(str))      :: string      
       Integer :: ic, i      
     END FUNCTION To_Upper
  END INTERFACE  
  !-------------------------------------------------------------
  integer                :: year
  character(len=120)     :: name(1995:2015,40)
  character(len=20)      :: nameUpper*20, nameAtlTC
  integer                :: count(1995:2015), ns(1995:2015)
  integer                :: i,iy, iN, j, Iyear

  integer                :: TCdate (400)
  real                   :: TCLat (400), TCLon(400)
  character (len=4)      :: TCName(400)
  real                   :: WestLon


  WestLon = -60.

  name = "missing"

  open (unit = 10, file = "aew_sheet.csv")
  do iy = 2015,1995,-1
     read (10,*,err=20,end=30) year, ns(year),(name(iy,i), i = 1,ns(year))     
20   continue
  end do
30 continue


  ! read the genesis location text file for all Atlantic storms
  ! this file genesis_locations.txt was created by the code
  ! genesis_location.ncl -> find it in the ibtracs subdir

  open (20, file = "genesis_locations.txt", form = "formatted", status = "old")

  iN = 1
  do i = 1, 4000
     read(20,*,end=10) TCname(i), TCdate(i), TCLon(i), TCLat(i)
     iN = iN + 1
     print *, TCname(i), TCdate(i), TCLon(i), TCLat(i)
  end do
  close(20)

10 continue

  open (30, file = "aew_tc_info.txt", form = "formatted", status = "new")
  
  count = 0
  do iy = 1995,2015
     do i = 1,40
        if ( name(iy,i) .ne. "missing" ) then
           count(iy) = count(iy) + 1
           nameUpper = name(iy,i)(1:20)
           nameUpper = to_upper(nameUpper)           
           print *, "checking for " , iy, nameUpper
           do j = 1,iN
              Iyear = TCdate(j)/1000000

! match the year and TC name
              if ( Iyear .eq. iy .and. TCname(j)(1:3) .eq. nameUpper(1:3)) then

! Now only write the info for TCs that formed west of specified longitude

                 if ( TCLon(j) .le. WestLon ) then
                    write (30,100) nameUpper(1:6), TCdate(j), TCLat(j), TCLon(j)
                 end if
                 
              end if
           end do   
        end if
     end do
     print *, iy, count(iy),ns(iy)
  end do


100 format (a6,2x,i10,2x,f8.3,2x,f8.3)

  
  end


!!$-------------------------------------------------------------------

  Function to_upper (str) Result (string)

!   ==============================
!   Changes a string to upper case
!   ==============================

    Implicit None
    Character(*), Intent(In) :: str
    Character(LEN(str))      :: string

    Integer :: ic, i

    Character(26), Parameter :: cap = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    Character(26), Parameter :: low = 'abcdefghijklmnopqrstuvwxyz'

!   Capitalize each letter if it is lowecase
    string = str
    do i = 1, LEN_TRIM(str)
        ic = INDEX(low, str(i:i))
        if (ic > 0) string(i:i) = cap(ic:ic)
    end do

End Function to_upper

!!$------------------------------------------------------------------
