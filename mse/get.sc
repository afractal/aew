#!/bin/csh
#################################################################
# Csh Script to retrieve 132 online Data files of 'ds627.1',
# total 276.94M. This script uses 'wget' to download data.
#
# Highlight this script by Select All, Copy and Paste it into a file;
# make the file executable and run it on command line.
#
# You need pass in your password as a parameter to execute
# this script; or you can set an environment variable RDAPSWD
# if your Operating System supports it.
#
# Contact davestep@ucar.edu (Dave Stepaniak) for further assistance.
#################################################################

set pswd = $1
if(x$pswd == x && `env | grep RDAPSWD` != '') then
 set pswd = $RDAPSWD
endif
if(x$pswd == x) then
 echo
 echo Usage: $0 YourPassword
 echo
 exit 1
endif
set v = `wget -V |grep 'GNU Wget ' | cut -d ' ' -f 3`
set a = `echo $v | cut -d '.' -f 1`
set b = `echo $v | cut -d '.' -f 2`
if(100 * $a + $b > 109) then
 set opt = 'wget --no-check-certificate'
else
 set opt = 'wget'
endif
set opt1 = '-O Authentication.log --save-cookies auth.rda_ucar_edu --post-data'
set opt2 = "email=aaiyyer@ncsu.edu&passwd=$pswd&action=login"
$opt $opt1="$opt2" https://rda.ucar.edu/cgi-bin/login
set opt1 = "-N --load-cookies auth.rda_ucar_edu"
set opt2 = "$opt $opt1 http://rda.ucar.edu/dsrqst/AIYYER270241/"
set filelist = ( \
  ei.mdfa.fc12hr.sfc.regn128sc.2000010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2000120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2001120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2002120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2003120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2004120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2005120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2006120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2007120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2008120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2009120100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010010100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010020100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010030100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010040100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010050100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010060100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010070100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010080100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010090100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010100100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010110100.aiyyer270241 \
  ei.mdfa.fc12hr.sfc.regn128sc.2010120100.aiyyer270241 \
)
while($#filelist > 0)
 set syscmd = "$opt2$filelist[1]"
 echo "$syscmd ..."
 $syscmd
 shift filelist
end

rm -f auth.rda_ucar_edu Authentication.log
exit 0