
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/diagnostics_cam.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/calendar_decode2.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"

begin


; regressed data
    dir = "./"
    fileName = "reg-filtered-700.nc"
    fileN = dir + fileName
    
    inV = addfile(fileN,"r")
    V = inV->rcAll
    printVarSummary(V)
    nlag = inV->nlag

    print (nlag)


    V = V

; Lets shift the longitudes so that we can plot the hovmoller 
; Also make time decrease
    
    VH    = dim_avg_n_Wrap( V(:,{8.:10.},:), 1 )  
    printVarSummary(VH)
    VH!0 = "time"


    V2 = VH
    V2!0 = "time"
    V2!1 = "lon"
    printVarSummary(V2)


    V2(:,0:119)      = VH(:,120:239)
    V2(:,120:239)    = VH(:,0:119)

    V2&lon(0:119) = VH&lon(120:239)  - 360.
    V2&lon(120:239) = VH&lon(0:119)  
     

    ntimes = 2*nlag + 1
    timeArray  = new ( (/ntimes/), float )

    do i = 0,ntimes-1
      timeArray(i) = (-1*nlag*6 + i*6)/24.
    end do



    V2&time = timeArray


;    dates = ut_calendar(EKH2&Time,-3)
;    times = EKH2&Time


    V2&lon@units = "degreesE"
;    V2@units = "m/s"
;    V2@long_name = "Regressed v-wind"
    printVarSummary(V2)

;   print (dates)
;    print (V2)

;************************************************
;************************************************
  wks = gsn_open_wks("ps" ,"hov")                ; open a ps file
; gsn_define_colormap(wks,"sunshine_diff_12lev")              ; choose colormap

  gsn_define_colormap(wks,"gsltod")             ; choose colormap

; reverse the first two colors
  setvalues wks            
    "wkColorMap"        : "gsltod"  
    "wkForegroundColor" : (/0.,0.,0./)  
    "wkBackgroundColor" : (/1.,1.,1./) 
  end setvalues

  colors = gsn_retrieve_colormap(wks)     ; retrieve color map for editing. 
; The 3rd color is the First color for shading!
  colors(2,:) = (/ 1., 1., 1. /)          ; replace the first color with white
  gsn_define_colormap(wks,colors)         ; redefine colormap to workstation, color map now includes a gray

  res                      = True               ; plot mods desired
  res@cnFillOn             = False               ; turn on color fill
  res@gsnSpreadColors      = True               ; use full range of colors

  res@tiMainString         = "  " ;[2-10 day Bandpass Filtered EKE]"   ; title

  res@cnLevelSelectionMode = "ManualLevels"     ; manual contour levels
  res@cnMinLevelValF       =   -6.               ; min level
  res@cnMaxLevelValF       =    6.               ; max level
  res@cnLevelSpacingF      =   .3                ; contour level spacing
  res@trYReverse          = True                ; reverse y axis

  res@cnLinesOn       = True           ; turn on contour lines
  res@cnLineLabelsOn  = False            ; turn off contour labels
  res@gsnDraw         =  False                   ; do not draw the plot
  res@gsnFrame        =  False                   ; do not advance the frame


  res@gsnContourZeroLineThicknessF = 0 	    ; eliminates zero contour
  res@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1




  res@trYReverse          = False               ; reverse y axis
  res@gsnMajorLonSpacing  = 15.

; from diaz
;  restick = True
;  restick@ttmFormat = "%c %D"
;  restick@ttmAxis = "YL"
;  restick@ttmMajorStride = 40
;  time_axis_labels(times,  res, restick )


  res@cnInfoLabelOn = False
  res@tiXAxisString   = "Longitude"  
  res@tiYAxisString   = "Lag (Days)"

  plot = gsn_csm_hov(wks, V2({-8.:8.},{-65.:30.}), res) 

  draw(plot)                     

  frame(wks)



end
