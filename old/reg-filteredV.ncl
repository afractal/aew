;--------------------------------------------------------------------------------------
; Unfiltered fields (e.g., u,v,PV) regressed against bandpassed reference Time Series
; The reference time series is created by a separate script: filt-refTS.ncl
;
; Output to binary file:
; 
;--------------------------------------------------------------------------------------
;--------------------------------------------------------------------------------------
;--------------------------------------------------------------------------------------
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
;
begin

 
; Read the reference TS
  fileTS = "filt-refTS-all-700.nc"
  ts  = addfile( fileTS, "r" )
  RefTSAll=ts->vAll

  printVarSummary( RefTSAll )



  lev = 700
  varF = "v"  ; u v g, w, t
;
; Set the variable name to read further down the code.
;
;
;
; nlag is not lag time in hours. if data interval is dh then the lag hours = nlag*dh
  nlag = 41

;
; 
  nyears = 21
  ystart = 1990
;  
; open the file
;
  icount = 0
;
;
;
  dir = "/data3/era-interim/plev/daily/grib/"
;
;
  do iy = ystart,ystart+nyears-1

;    print ( iy ) 
;
;
    icount = icount + 1
    files    = new(3,string)
    files(0) = dir + varF +"." + iy + "07.grib"
    files(1) = dir + varF +"." + iy + "08.grib"
    files(2) = dir + varF +"." + iy + "09.grib"
;  
    in = addfiles ( files,"r")
    time=doubletoint(in[:]->initial_time0_encoded)
;    print ( time) 
;
;
; Define the JAS period 
    sDate = iy*1000000 + 070100
    eDate = iy*1000000 + 093018
    nt3 = get1Dindex( time, sDate)
    nt4 = get1Dindex( time, eDate)
    
;    print ( iy + " " + nt3 + " " + nt4)

    nhrs = nt4-nt3+1
    D  = in[:]->V_GDS0_ISBL(nt3:nt4,{lev},{-10:35},:)
;
    delete (in)
    delete (files)
;
    if ( icount .eq. 1 )       
      
      dims = dimsizes ( D )
      ny  =  dims(1)
      nx  =  dims(2)
      NT = nyears*nhrs 
      Dall  = new ( (/NT,ny,nx/), typeof ( D ) )
      printVarSummary(D)
      
    end if
;    
    i0 =  (icount-1)*nhrs
    i1 = i0 + nhrs - 1
;    
;    
    Dall(i0:i1,:,:)  = D
;    

    delete ( D )

    delete ( time )
  end do
;  
;
  print ( "Read All Data For All Years" ) 
;
  Dall!0 = "time"
  Dall!1 = "lat"
  Dall!2 = "lon"
;

; Now lets regress the data
  printVarSummary (Dall)



  
  
  do j = 0,ny-1
    do i = 0,nx-1

      RefTS =  RefTSAll (:,j,i)
      dims = dimsizes ( RefTS)
                                ; open the file to write to
      fileName = "reg-filtered" + "_" + i + "_" + j + ".nc"
      system( "rm " + fileName )
      setfileoption( "nc", "Format", "LargeFile" )
      outFile = addfile( fileName, "c" )

      ilag = 0
      do ih = -nlag,nlag,1
        
; define the new arrays to hold the lagged data
; Since we have seasonal data, we will lose ih points for each year
        NYL = (nhrs - abs(ih))
;
; NT defines the total number of temporal points over nyears
;
        NT = nyears*NYL
;
; create new arrays to hold the time series and data to be regressed
        A  = new ( (/NT/), typeof ( RefTS) )      
        B  = new ( (/NT,ny,nx/), typeof ( Dall ) )
;    
; fill the arrays with lagged data
;  
        ic = 0
        do iy = ystart,ystart+nyears-1
; Here we determine the indices of the original data that should be used to fill the arrays A,B,C   
          if ( ih .ge. 0 ) then
;
; For positive lag, the reference time series is Behind in time relative to the data to be regressed
; So we shift the data forward
;
            nSA = ic*nhrs                ; reference TS not shifted 
            nEA = (ic+1)*nhrs - ih - 1
            nSB = ic*nhrs + ih           ; data shifted forward
            nEB = (ic+1)*nhrs - 1   
            i1 = ic*NYL
            i2 = (ic+1)*NYL -1
            A(i1:i2)     = RefTS(nSA:nEA)
            B(i1:i2,:,:) =  Dall(nSB:nEB,:,:)
          end if
          
          if ( ih .lt. 0 ) then
;
; For negative lag, the reference time series is Ahead in time relative to the data to be regressed
; So we shift the reference TS forward (-ih is positve!)
;
            nSA = ic*nhrs - ih
            nEA = (ic+1)*nhrs - 1 
            nSB = ic*nhrs
            nEB = (ic+1)*nhrs + ih - 1
            i1 = ic*NYL
            i2 = (ic+1)*NYL -1
            A(i1:i2)     = RefTS(nSA:nEA)
            B(i1:i2,:,:) =  Dall(nSB:nEB,:,:)
            
          end if
          
          ic = ic + 1    
        end do
        
; OK. We have filled the arrays A,B and C. Now we regress the data
        
; OK. We have filled the arrays A,B and C. Now we regress the data
; define arrays for the tval and npts
        
        tval       =  new ( (/ny,nx/),  typeof ( A ) )
        nptsxy     =  new ( (/ny,nx/), integer )
        rc = regcoef(A,B(lat|:,lon|:,time|:),tval,nptsxy)
        
        
; compute the std dev of A and multiply the regression coeff by it
;    stdA = dim_stddev_Wrap( A )
;    rc = rc*stdA
;    print ( "Lag = " + ih + " stdA =" + stdA )
        
;   Copies all named dimensions and coordinate variables from one variable 
;   to another except for the rightmost dimension. 
        
        copy_VarCoords_1 ( B(lat|:,lon|:,time|:), rc)
        
        
        df    = nptsxy-2   ; degrees of freedom
        bb     = tval            ; b must be same size as tval (and df)
        bb     = 0.5
        prob  = betainc(df/(df+tval^2),df/2.0,bb)
        copy_VarCoords_1 (rc,prob)
        
; Create the arrays to hold the regressed data
        if ( ilag .eq. 0 )then
          rcAll   = new ( (/2*nlag+1,ny,nx/), typeof ( rc ) )
          prAll   = new ( (/2*nlag+1,ny,nx/), float )
        end if
;        
        prAll(ilag,:,:) = prob
        rcAll(ilag,:,:) = rc
        
        ilag = ilag + 1
        
        delete (A)
        delete (B)
        delete (rc)
        delete (tval)
        delete (nptsxy)
        delete (df)
        delete (bb)
        
      end do

      outFile->nlag = nlag
      outFile->rcAll = rcAll
      outFile->prAll = prAll
      delete (outFile)

    end do
  end do



;-------------------------------------------------------------------------------
end