;-------------------------------------------------------------
; NCSU Tropical Dynamics Group
;------------------------------------------------------------
;A. Aiyyer 04.28.2019
;
; This program reads the seasonal average 2-12 day trmm, eke

;-------------------------------------------------------------
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

load "../../reg/column_avg.ncl"

begin






  baseDir  = "/home/anant/data50/data/trmm/"
  fileName = "trmm3b42_daily_2to12day_filtered_JAS_variance.nc"
  infile = baseDir + fileName
  inF = addfile (infile, "r" )
  trmmVar=inF->trmmVariance
  printVarSummary(trmmVar)



  
  trmmVar@units = " " ; blank out the units. actual: (mm/hr)^2

  trmmVar = trmmVar*24.*24. ; concert to mm/day
  
; also read the yearly EKE and average it for climo

  ; first average the eke for each year
  icount = 0.
  do iy = 1998,2014
     print ( "Year = " + iy )     
     infile = "~/data50/data/eke/eke"+iy + ".jas.nc"
     inF = addfile (infile, "r" )
     EKEAVE = inF->EKEAVE({1000:200},:,:)
     EKEAVE&lon@units = "degrees-east"
     EKEAVE&lat@units = "degrees-north"
     if ( icount .eq. 0. ) then
       EM = EKEAVE
     end if     
     icount = icount + 1
     EM = EM*(icount - 1)/icount + EKEAVE/icount
     delete(inF)
     delete(EKEAVE)
  end do
  EM@long_name = "EKE"
  EM@standard_name = "EKE"
  


  wks = gsn_open_wks("pdf" ,"trmmVariance")                      ; ps,pdf,x11,ncgm,eps

  gsn_define_colormap(wks,"GMT_cool")


  
  resTrmm                     = True
  resTrmm@gsnDraw             = False           ; don't draw
  resTrmm@gsnFrame            = False           ; don't advance frame
  resTrmm@cnInfoLabelOn       = False           ; turn off cn info label

  resTrmm@mpMinLonF          = -50           ; choose a subregion
  resTrmm@mpMaxLonF          =  50.
  resTrmm@mpMinLatF          =  -5.           ; choose a subregion
  resTrmm@mpMaxLatF          =  35.
  resTrmm@pmTickMarkDisplayMode = "Always"
  resTrmm@mpFillOn              =  False          ; turn off map fill
  resTrmm@mpOutlineDrawOrder    = "PostDraw"      ; draw continental outline last
  resTrmm@cnFillOn             = True               ; turn on color for contours
  resTrmm@cnLinesOn            = False              ; turn off contour lines
  resTrmm@cnLineLabelsOn       = False              ; turn off contour line labels


  resTrmm@mpPerimOn              = True
  resTrmm@mpFillOn               = False
  resTrmm@mpLabelsOn             = False
  resTrmm@lbLabelBarOn = True
  resTrmm@gsnAddCyclic = True
 
  resKE                     = True
  resKE@gsnDraw             = False           ; don't draw
  resKE@gsnFrame            = False           ; don't advance frame
  resKE@cnInfoLabelOn       = False           ; turn off cn info label
  resKE@gsnAddCyclic = True
  resKE@cnFillOn             = False               ; turn on color for contours
  resKE@cnLinesOn            = True             ; turn off contour lines
  resKE@cnLineLabelsOn       = False              ; turn off contour line labels
  resKE@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels

  resKE@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1

  resKE@cnMinLevelValF       =  2.               ; set min contour level
  resKE@cnMaxLevelValF       =  16.                 ; set max contour level
  resKE@cnLevelSpacingF      =  2.               ; set contour spacing

  



  resKE@gsnLeftString   = " "
  resKE@gsnCenterString = " "
  resKE@gsnRightString  = " " 

  resTrmm@gsnLeftString   = " "
  resTrmm@gsnCenterString = " "
  resTrmm@gsnRightString  = " " 


  
  resP                     = True                ; modify the panel plot
  resP@lbLabelFontHeightF  = 0.007               ; make labels smaller 

  plot = new(2,graphic)
  resP@txString= "  "; "July-September Average"


  ;resP@gsnMaximize = True

  
  resTrmm@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
  resTrmm@cnMinLevelValF       =  20.               ; set min contour level
  resTrmm@cnMaxLevelValF       =  400.                 ; set max contour level
  resTrmm@cnLevelSpacingF      =  40.               ; set contour spacing

  resTrmm@mpGeophysicalLineThicknessF  = 2.

  resTrmm@gsnLeftString   = "(a) 2-12 Day Trmm Variance and 650 hPa EKE"
  plot1 = gsn_csm_contour_map_ce(wks,trmmVar,resTrmm)
  plot2 = gsn_csm_contour(wks,EM({650},:,:),resKE)
  overlay(plot1,plot2)
  plot(0) = plot1

  resTrmm@gsnLeftString   = "(b)  2-12 Day Trmm Variance and 850 hPa EKE"
  plot1 = gsn_csm_contour_map_ce(wks,trmmVar,resTrmm)
  plot2 = gsn_csm_contour(wks,EM({850},:,:),resKE)
  overlay(plot1,plot2)
  plot(1) = plot1
 

  
  gsn_panel(wks,plot,(/2,1/),resP)               ; now draw as one plot

end






