;--------------------------------------------------------------------------------------
; This program will read the regression data created by ../src/regress_fields.ncl
; and plot Q-vectors and their convergence
;
; ++++++++++++++++++++Needs edits. esp. replace Ug,Vg by Upsi, Vpsi
;
;
; This code must be used in conjunction with regress_fields.ncl
;
;--------------------------------------------------------------------------------------
; NCSU Tropical Dynamics Group
; Aiyyer 12/2018
;--------------------------------------------------------------------------------------
;--------------------------------------------------------------------------------------
;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "/home/anant/code/ncl/cyl_calc.ncl"


begin


  level = 850.
  
  daily = True  ; for this daily has to be set True
                ; since we base this analysis on daily era data
                ; Aiyyer 12/2018

  
; select the reference variable name
; the regressed data will be in the file name of the form
; fileName = baseDir+"Reg_"+var1+"_{refVar}{filt}_levRef"+levRef+"latRef"+latRef+".nc"
  
;--------------------------------------------------------------------------
; refVar = "Vfilt"
; select the reference time series attributes
;  latRef = 10
;  levRef = 650
;--------------------------------------------------------------------------

  
  refVar = "TRMMfilt" ; trmm 2-12 day filtered base time series
  ; select the reference time series attributes
  latRef = 10
  levRef = 0


  
; choose the base point to plot for.  
  lonRefIndex = 1
  ; the lonRef is based on the index
  lonRef     = -40. + lonRefIndex*20.
  ll = abs(lonRef)
  EW = "E"
  if (lonRef .lt. 0. ) then
    EW = "W"
  end if

; the plot will be produced in this pdf file
  outPutFile = "qvec_ref_" + latRef+"N_"+ll+EW+"_"+levRef+"hPa"
  print ( outPutFile )
 
; define constants  
  L  = 2.501e6
  cp = 1005.7
  grav = 9.8
  rhoW = 1.e3 ; density of water



; ------------------------------------------------------------------------
; Now read the AEW scale eddies, i.e. the perturbation data
  baseDir = "~/data50/data/reg/"
  if (daily) then
    baseDir = "~/data50/data/reg/daily/"
  end if

; as for now this is hard coded. May need to read this direcly from the reg files  
  ntimes = 25
  dh     =  6
  if (daily) then
    ntimes = 7 ; 25
    dh     = 24 ; 6
  end if
  
; determine the lag 0 hour ; also hard coded
  l0 = (ntimes-1)/2

; read the winds at zero lag
  fileVariable1 = "rcAll"
  fileSigniVar1 = "prAll"

  var1 = "U"
  fileName = baseDir+"Reg_"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  if (daily) then
    fileName = baseDir+"Reg_daily"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  end if
  inFile = addfile( fileName, "r" )
  UP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)
;
; read the winds
  var1 = "V"
  fileName = baseDir+"Reg_"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  if (daily) then
    fileName = baseDir+"Reg_daily"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  end if
  inFile = addfile( fileName, "r" )
  VP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)
;

  ; read the T
  var1 = "T"
  fileName = baseDir+"Reg_"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  if (daily) then
    fileName = baseDir+"Reg_daily"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  end if
  inFile = addfile( fileName, "r" )
  TP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)
  ;prAll1=inFile->$fileSigniVar1$({level},:,:,:,lonRefIndex)  


  ; read the Z
  var1 = "Z"   ; should be geopotential
  fileName = baseDir+"Reg_"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  if (daily) then
    fileName = baseDir+"Reg_daily"+var1+"_"+refVar+"_levRef"+levRef+"latRef"+latRef+".nc"
  end if
  inFile = addfile( fileName, "r" )
  ZP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)
  ;prAll1=inFile->$fileSigniVar1$({level},:,:,:,lonRefIndex)  




  
  printVarSummary(UP)
  printVarSummary(VP)
  printVarSummary(TP)
  printVarSummary(ZP)


  c_pd = 1004. ; specific heat at constant pressure for air [approx 1004 J/(kg-K)]
  R = 287. ; specific gas constant for air [J/(kg-K)]
  Rcpd = R/c_pd
  p0 = 100000.
  L_v = 2400000.
  
  lat  = UP&lat
  lon  = UP&lon
  plev = UP&level

  if ( plev@units .eq. "hPa") then
    plev = plev*100
    plev@units  = "Pa"
  else
    print ( plev@units )
    print ("Check units of plev. Terminating code")
    exit
  end if
  
  uvg = z2geouv(ZP,lat,lon,0)
  Ug = uvg(0,:,:,:)*.1 ; multiply by .1 since Z is already gZ
  Vg = uvg(1,:,:,:)*.1 ; multiply by .1 since Z is already gZ

  
  copy_VarCoords(UP,Ug)
  copy_VarCoords(VP,Vg)


  printVarSummary(Ug)

; perturbation potential temperature 
; Theta = pot_temp(plev ,TP , 0, False)
;  printVarSummary(Theta)
;  print ( TP(:,{10.},{10.}) + " " + Theta(:,{10.},{10.}) ) 
;  gradTheta = grad_latlon_cfd (Theta,lat,lon, False, False) ; not cyclic
;  ThetaY = gradTheta[0]
;  ThetaX = gradTheta[1]

   
  gradT = grad_latlon_cfd (TP,lat,lon, False, False) ; not cyclic
  TY = gradT[0]
  TX = gradT[1]

 

  gradU = grad_latlon_cfd (UP,lat,lon, False, False) ; not cyclic
  gradV = grad_latlon_cfd (VP,lat,lon, False, False) ; not cyclic
  UX = gradU[1]
  UY = gradU[0]
  VX = gradV[1]
  VY = gradV[0]


  Q1 = -1.*R*(UX*TX + VX*TY)/(level*100.)
  Q2 = -1.*R*(UY*TX + VY*TY)/(level*100.)


  
  copy_VarCoords(UP,Q1)
  copy_VarCoords(UP,Q2)

; Q vector divergence
  Qdiv = uv2dv_cfd (Q1,Q2,lat,lon,2)
  
  copy_VarCoords(UP,Qdiv)


  printVarSummary(Qdiv)

  print(min(Qdiv) + " " + max(Qdiv))
  print(min(Q1) + " " + max(Q1))
  print(min(Q2) + " " + max(Q2))


  Qdiv = Qdiv*1.e21
  Q1 = Q1*1.e14
  Q2 = Q2*1.e14
  
  wks = gsn_open_wks("pdf", "qvec.pdf")                      ; ps,pdf,x11,ncgm,eps
  resP                     = True                ; modify the panel plot    


  resP@gsnPanelYWhiteSpacePercent = 8
  resP@gsnPanelXWhiteSpacePercent = 8


  colorMapC  = "CBR_wet"  
  colorMapP  = "BlueDarkRed18"  
  colorMapC=colorMapP
  
; THIS IS FOR THE SHADED FIELD
  res                     = True
  res@gsnDraw             = False           ; don't draw
  res@gsnFrame            = False           ; don't advance frame
  res@cnInfoLabelOn       = False           ; turn off cn info label
  res@mpMinLonF          = lonRef-30.           ; choose a subregion
  res@mpMaxLonF          = lonRef+30.
  res@mpMinLatF          =  1.           ; choose a subregion
  res@mpMaxLatF          =  25.
  res@pmTickMarkDisplayMode = "Always"
  res@mpFillOn              =  False          ; turn off map fill
  res@mpOutlineDrawOrder    = "PostDraw"      ; draw continental outline last
  res@cnFillOn             = False               ; turn on color for contours
  res@cnLinesOn            = True              ; turn off contour lines
  res@cnLineLabelsOn       = False              ; turn off contour line labels
  res@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
 
  res@gsnAddCyclic = False
  res@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
  res@gsnContourZeroLineThicknessF = 0. 


; THIS IS FOR THE CONTOURED FIELD
  res1 = True
  res1@cnFillOn             = False               ; turn on color for contours
  res1@cnLinesOn            = True              ; turn off contour lines
  res1@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
  res1@gsnDraw             = False           ; don't draw
  res1@gsnFrame            = False           ; don't advance frame
  res1@cnInfoLabelOn       = False           ; turn off cn info label
  res1@gsnAddCyclic = False
  res1@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
  res1@gsnContourZeroLineThicknessF = 0. 
  res1@cnLineLabelsOn       = False              ; turn off contour line labels

;  res1@gsnLeftStringFontHeightF   = 0.02		; instead of using txFontHeightF or gsnStringFontHeightF 
;  res1@gsnCenterStringFontHeightF = 0.02			; to set the gsnLeft/Center/RightString font heights,
;  res1@gsnRightStringFontHeightF  = 0.02		; individually set each string's font height.


; THIS IS FOR THE CONTOURED FIELD
  res2 = res1
  res2@cnFillOn             = False               ; turn on color for contours
  res2@cnLinesOn            = True              ; turn off contour lines
  res2@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
  res2@gsnContourZeroLineThicknessF = 2. 
  res2@gsnAddCyclic = False

  
; vector resources
    ;res@vcRefAnnoOrthogonalPosF  = .1               ; move ref vector down
    ;res@vcRefAnnoOrthogonalPosF = -1.0   ; move ref vector block up
  resVecCon = res
  resVecCon@vcRefAnnoOn = False ; remove the ref vector block
  resVecCon@vcRefMagnitudeF  = .5
  resVecCon@vcRefLengthF     = 0.05  
  resVecCon@vcGlyphStyle      = "CurlyVector"    ; turn on curly vectors
  resVecCon@vcMinDistanceF   = 0.02            ; thin vectors
  resVecCon@gsnScalarContour     = True               ; contours desired


  cmapVecCon       = read_colormap_file(colorMapP)
  resVecCon@cnFillColors = cmapVecCon


  resVecCon@gsnLeftString = " " + level + "hPa Q-vec"
  resVecCon@cnMinLevelValF       = -20.               ; set min contour level
  resVecCon@cnMaxLevelValF       =  20.                 ; set max contour level
  resVecCon@cnLevelSpacingF      =  2.5              ; set contour spacing
  plot2 = gsn_csm_vector_scalar_map(wks,Q1({level},:,:),Q2({level},:,:),Qdiv({level},:,:), resVecCon)
  draw(plot2)




  frame(wks)


  

  resVecCon@gsnLeftString = " " + level + "Vg Z"
  resVecCon@cnMinLevelValF       = -10.               ; set min contour level
  resVecCon@cnMaxLevelValF       =  10.                 ; set max contour level
  resVecCon@cnLevelSpacingF      =   2.              ; set contour spacing
  plot2 = gsn_csm_vector_scalar_map(wks,Ug({level},:,:),Vg({level},:,:),ZP({level},:,:), resVecCon)
  draw(plot2)
  frame(wks)



  resVecCon@gsnLeftString = " " + level + "hPa V T"
  resVecCon@cnMinLevelValF       = -.25               ; set min contour level
  resVecCon@cnMaxLevelValF       =  .25                 ; set max contour level
  resVecCon@cnLevelSpacingF      =  .05              ; set contour spacing
  plot2 = gsn_csm_vector_scalar_map(wks,UP({level},:,:),VP({level},:,:),TP({level},:,:), resVecCon)
  draw(plot2)
  frame(wks)



  

;------------------------------------------------------------------------------------------------


  
end
