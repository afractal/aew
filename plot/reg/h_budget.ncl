; a program to calculate terms in the  mse budgets using
; regression data
;
;
;
; A. Aiyyer NCSU Tropical Dynamics Group
; 8.30.2018
;
;Note:
;
; The flux data (SLHF,SSHF,TTR...) are in units of W/m^2 s
; since they are accumulated over a day
;
; We divide by 24x3600 to get the data in W/m^2
;
;-----------------------------------------------------------------------

; load some functions for calclating spatial derivatives

load "/home/anant/work/code/ncl/cyl_calc.ncl"
load "./column_avg.ncl"

begin

; if needed set values below surface to zero.  
  topocheck = True
;   
  daily = True  ; for this daily has to be set True
                ; since we base this analysis on daily era data
                ; Aiyyer 12/2018
;
;
;--------------------------------------------------------------------------
; refVar = "Vfilt"
; select the reference time series attributes
;  latRef = 10
;  levRef = 650
;--------------------------------------------------------------------------
  
  refVar = "trmm" ; trmm 2-12 day filtered base time series
  refVar = "gpcp" ; gpcp 2-10 day filtered base time series

  ; select the reference time series attributes
  latRef = 10
  levRef = 0
;-------------------------------------------------------------------------

  
  ;
  lonRefIndex = 4
  ; the lonRef is based on the index
  lonRef     = -40. + lonRefIndex*10.

  ll = abs(lonRef)
  EW = "E"
  if (lonRef .lt. 0. ) then
    EW = "W"
  end if
; the plot will be produced in this pdf file
  outPutFile = "h_budget_ref_" + latRef+"N_"+ll+EW+"_"+levRef+"hPa"
  print ( outPutFile )

  outPutFile2 = "d_budget_ref_" + latRef+"N_"+ll+EW+"_"+levRef+"hPa"

; define constants  
  L  = 2.501e6
  cp = 1005.7
  grav = 9.8
  rhoW = 1.e3 ; density of water
  

;  read era surface elevation

  fileName = "~/data50/data/era/era_elevation.75deg.nc"
  inFile = addfile( fileName, "r" )
  sfcH = lonFlip(inFile->z) ; this is sfc elevation as geopotential
  sfcH := short2flt(sfcH(0,{-45.:45.},:))
 
  print(sfcH({20.},{20.}))

; needed to change fillvalue
  sfcH@_FillValue = 9.96921e+36
  sfcH@missing_value  = sfcH@_FillValue


  
; first read the basic state variables
  baseDir = "/home/anant/data50/data/era/"
  fileName = "jas_era_climo_1980_2010.nc"                       
  infile = baseDir + fileName
  inF = addfile (infile, "r" )
  Q = lonFlip(inF->Q({1000.:100.},{-40:40},:))
  T = lonFlip(inF->T({1000.:100.},{-40:40},:))
  Z = lonFlip(inF->Z({1000.:100.},{-40:40},:))
  U = lonFlip(inF->U({1000.:100.},{-40:40},:))
  V = lonFlip(inF->V({1000.:100.},{-40:40},:))
  W = lonFlip(inF->W({1000.:100.},{-40:40},:))
  U := U(:,:,{-90.:90.})
  V := V(:,:,{-90.:90.})
  Q := Q(:,:,{-90.:90.})
  Z := Z(:,:,{-90.:90.})
  W := W(:,:,{-90.:90.})
  T := T(:,:,{-90.:90.})

  
 ; needed to change fillvalue
  Q@_FillValue = 9.96921e+36
  Q@missing_value  = Q@_FillValue

;  printVarSummary(U)
  
;----------------------------------------------------------------------------------------------------
  baseDir = "~/data50/data/reg/"
  if (daily) then
    baseDir = "/home/anant/data50/data/reg/trmmBased/"
    baseDir = "/home/anant/data50/data/reg/gpcp/"
  end if
  
 
  ntimes = 25
  dh     =  6
  if (daily) then
    ntimes = 7 ; 25
    dh     = 24 ; 6
  end if


; determine the lag 0 hour

  l0 = (ntimes-1)/2


  
; read the winds at zero lag
  fileVariable1 = "rcAll"
  fileSigniVar1 = "prAll"
  var1 = "U"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  UP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)

 ; printVarSummary(UP)
 ; exit
  
; read the winds
  var1 = "V"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  VP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)


; read the winds
  var1 = "W"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  WP=inFile->$fileVariable1$(:,:,:,l0,lonRefIndex)

  
; read the Q
  var1 = "Q"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  QP=inFile->$fileVariable1$(:,:,:,l0-1:l0+1,lonRefIndex)
  ;prAll1=inFile->$fileSigniVar1$({level},:,:,:,lonRefIndex)  

; read the Z
  var1 = "Z"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  ZP=inFile->$fileVariable1$(:,:,:,l0-1:l0+1,lonRefIndex)
  ;prAll1=inFile->$fileSigniVar1$({level},:,:,:,lonRefIndex)  


; read the T
  var1 = "T"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  TP=inFile->$fileVariable1$(:,:,:,l0-1:l0+1,lonRefIndex)
  ;prAll1=inFile->$fileSigniVar1$({level},:,:,:,lonRefIndex)  

  
; read the SLHF
  var1 = "SLHF"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  SLHF=inFile->$fileVariable1$(:,:,l0,lonRefIndex)
  printVarSummary (SLHF)
; In ERA interim, SLHF is positive downward
; So we need to multiply by -1 so that positive values are upward.
  SLHF = -1*SLHF
  
; read the SSHF
  var1 = "SSHF" 
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  SSHF=inFile->$fileVariable1$(:,:,{0},lonRefIndex)
  printVarSummary (SSHF)
; In ERA interim, SSHF is positive downward
; So we need to multiply by -1 so that positive values are upward.
  SSHF = -1*SSHF
  
; read the TTR
  var1 = "TTR"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  TTR=inFile->$fileVariable1$(:,:,l0,lonRefIndex)
  printVarSummary (TTR)
 
  
; read the STR
  var1 = "STR"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  STR=inFile->$fileVariable1$(:,:,l0,lonRefIndex)
  printVarSummary (STR)
 

;read the TSR
  var1 = "TSR"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  TSR=inFile->$fileVariable1$(:,:,l0,lonRefIndex)
  printVarSummary (TSR)
 


  ;read the SSR
  var1 = "SSR"
  fileName = baseDir+  refVar + "_"+var1+"_"+"latRef"+latRef+".nc"
  inFile = addfile( fileName, "r" )
  SSR=inFile->$fileVariable1$(:,:,l0,lonRefIndex)
  printVarSummary (SSR)
  
  
  printVarSummary(U)
  printVarSummary(UP)
  printVarSummary(Q)
  printVarSummary(QP)


  U!0 = "level"
  U!1 = "lat"
  U!2 = "lon"
  V!0 = "level"
  V!1 = "lat"
  V!2 = "lon"
  Q!0 = "level"
  Q!1 = "lat"
  Q!2 = "lon"


  
  Q    =  Q*L
  QP   = QP*L


  
  lat=U&lat
  lon=U&lon
  dims = dimsizes(U)
  nz = dims(0)  
  plevels = U&level


; interpolate elevation from .75 deg to the .703 era grid

  lonS := sfcH&longitude
  latS := sfcH&latitude  
  sfc = linint2_Wrap(lonS,latS,sfcH,True,lon,lat,0)
  printVarSummary(sfc)

  do k = 0,4 ;check the lower levels to see if below sfc and if so set to zero
    Q(k,:,:)  =where ((Z(k,:,:).lt.sfc),0.,Q(k,:,:))
    U(k,:,:)  =where ((Z(k,:,:).lt.sfc),0.,U(k,:,:))
    V(k,:,:)  =where ((Z(k,:,:).lt.sfc),0.,V(k,:,:))
    VP(k,:,:) =where ((Z(k,:,:).lt.sfc),0.,VP(k,:,:))
    UP(k,:,:) =where ((Z(k,:,:).lt.sfc),0.,UP(k,:,:))
    WP(k,:,:) =where ((Z(k,:,:).lt.sfc),0.,WP(k,:,:))
  end do
  do k = 0,5
    print ( k + " " + plevels(k) + " " + Z(k,{20.},{20.}) +  " " + sfc({20.},{20.}) + " " + Q(k,{20.},{20.}) )
  end do
  do k = 0,4 ;check the lower levels to see if below sfc
    do ilag = 0,2
      QP(k,:,:,ilag)=where ( (Z(k,:,:).lt.sfc),0.,QP(k,:,:,ilag))
    end do
  end do
  


  ; MSE 
  H = Q
  H = Q + Z + cp*T 
  HP = QP
  HP = ZP + cp*TP + QP
  printVarSummary(HP)

  
  
  H!0 = "level"
  H!1 = "lat"
  H!2 = "lon"
  HP!0 = "level"
  HP!1 = "lat"
  HP!2 = "lon"
  UP!0 = "level"
  UP!1 = "lat"
  U!2 = "lon"


  ; DSE 
  D = H
  D = Z + cp*T 
  DP = ZP
  DP = ZP + cp*TP 
  print (H(:,{10.},{-40.}) + " " + D(:,{10.},{-40.}) )

  
; first column average the HP at each lag

  HPC = HP(0,:,:,:)
  DPC = DP(0,:,:,:)

  do il = 0,2
    HPC(:,:,il) = column_avg(HP(:,:,:,il),plevels)
    DPC(:,:,il) = column_avg(DP(:,:,:,il),plevels)
  end do

; Calculate the HPC tendency
  HPCT =   HPC(:,:,1)
  HPCT =  (HPC(:,:,2) - HPC(:,:,0))/2. ;tendency in Joules per sq-m per day
  DPCT =   DPC(:,:,1)
  DPCT =  (DPC(:,:,2) - DPC(:,:,0))/2. ;tendency in Joules per sq-m per day


  
  printVarSummary(HPCT)
; reset column average HP to the lag zero data only
  HPC := HPC(:,:,1)
  DPC := DPC(:,:,1)

; reset MSE to just retain the lag zero data
  HP := HP(:,:,:,1) ;p,lat,lon
  DP := DP(:,:,:,1) ;p,lat,lon

  
; vertical advection of MSE
; vertical advection

  HH = HP + H ; includes the nonlinear term now
  DD = DP + D ; includes the nonlinear term now
  advWHP = WP
  advWDP = WP
  
  print (plevels)
  do k = 1,nz-2
    delP =(plevels(k+1) - plevels(k-1))*100.
    advWHP(k,:,:) = -1.*WP(k,:,:)* (HH(k+1,:,:) - HH(k-1,:,:))/delP
    advWDP(k,:,:) = -1.*WP(k,:,:)* (DD(k+1,:,:) - DD(k-1,:,:))/delP
  end do 
  k = 0
  delP =(plevels(k+1) - plevels(k))*100.
  advWHP(k,:,:) = -1.*WP(k,:,:)* (HH(k+1,:,:) - HH(k,:,:))/delP
  advWDP(k,:,:) = -1.*WP(k,:,:)* (DD(k+1,:,:) - DD(k,:,:))/delP

  advWHPC = column_avg(advWHP,plevels)
  advWHPC = advWHPC*24.*3600.
  advWDPC = column_avg(advWDP,plevels)
  advWDPC = advWDPC*24.*3600.


  

; Horizontal advection of MSE

  
; horizontal adv    -(Vbar . del HP + VP . del HP)
  advHP = HP
  advHP = 0.
  advDP = DP
  advDP = 0.
  
  do k =0,nz-1
    data = HP(k,:,:)
    dQx  = data
    dQy  = data
    dQx = ddx(lon,lat,data)
    dQy = ddy(lat,data)
    advHP(k,:,:) = -1.*U(k,:,:)*dQx - 1.*V(k,:,:)*dQy
    advHP(k,:,:) = advHP(k,:,:) -1.*UP(k,:,:)*dQx - 1.*VP(k,:,:)*dQy

    data = DP(k,:,:)
    dQx  = data
    dQy  = data
    dQx = ddx(lon,lat,data)
    dQy = ddy(lat,data)
    advDP(k,:,:) = -1.*U(k,:,:)*dQx - 1.*V(k,:,:)*dQy
    advDP(k,:,:) = advDP(k,:,:) -1.*UP(k,:,:)*dQx - 1.*VP(k,:,:)*dQy
  end do


  
; horizontal adv  -(U' dH )
; advection of mean H by wave scale winds
  
  advHP2 = HP
  advHP2 = 0.
  advDP2 = DP
  advDP2 = 0.
  do k =0,nz-1
    data = H(k,:,:)
    dQx  = data
    dQy  = data
    dQx = ddx(lon,lat,data)
    dQy = ddy(lat,data)
    advHP2(k,:,:) =  -1.*UP(k,:,:)*dQx - 1.*VP(k,:,:)*dQy
    data = D(k,:,:)
    dQx  = data
    dQy  = data
    dQx = ddx(lon,lat,data)
    dQy = ddy(lat,data)
    advDP2(k,:,:) =  -1.*UP(k,:,:)*dQx - 1.*VP(k,:,:)*dQy
  end do

  advHP = advHP + advHP2
  advHPC = column_avg(advHP,plevels)
  advHPC = advHPC*24.*3600. ;now units of J m^-2 day^-1
  
  advDP = advDP + advDP2
  advDPC = column_avg(advDP,plevels)
  advDPC = advDPC*24.*3600. ;now units of J m^-2 day^-1 

; Net   
  HPC  = HPC*1.e-6
  HPCT = HPCT*1.e-6
  advWHPC = advWHPC*1.e-6
  advHPC  = advHPC*1.e-6
  
  DPC  = DPC*1.e-6
  DPCT = DPCT*1.e-6
  advWDPC = advWDPC*1.e-6
  advDPC  = advDPC*1.e-6

  ngms = advWHPC
  ngms = (advWHPC + advHPC)/advWDPC


; we should divide the fluxes by 24*3600 to yield watts per m^2
; but since all rates (tendency and advection) are calculated per day
; in this code, we will simply cancel the two factors. so no need to scale
; except by 1.e-6 for plotting purpose

; Net radiative forcing   
  QNet = TTR

;   
  QNet = (TTR - STR) + (TSR - SSR)

  QNet = QNet*1.e-6

  SLHF = SLHF*1.e-6
  SSHF = SSHF*1.e-6
  
  SFLUX = SLHF
  SFLUX = (SFLUX + SSHF)
  
  resid = HPCT
  resid = HPCT - advWHPC - advHPC - QNet - SFLUX
  
  printVarSummary(QNet)
  
  print (min(QNet) + " " + max(QNet))

  print (min(SLHF) + " " + max(SLHF))



  colorMap  = "BlueDarkRed18"

  wks = gsn_open_wks("pdf" ,outPutFile)                      ; ps,pdf,x11,ncgm,eps
  gsn_define_colormap(wks,colorMap)
  
  

; for the shades
  res                     = True
  res@gsnDraw             = False           ; don't draw
  res@gsnFrame            = False           ; don't advance frame
  res@cnInfoLabelOn       = False           ; turn off cn info label
  res@mpMinLonF          = lonRef-40.           ; choose a subregion
  res@mpMaxLonF          =  lonRef+40.
  res@mpMinLatF          = -5.           ; choose a subregion
  res@mpMaxLatF          =  30.
  res@pmTickMarkDisplayMode = "Always"
  res@mpFillOn              =  False          ; turn off map fill
  res@mpOutlineDrawOrder    = "PostDraw"      ; draw continental outline last
  res@cnFillOn             = True               ; turn on color for contours
  res@cnLinesOn            = False              ; turn off contour lines
  res@cnLineLabelsOn       = False              ; turn off contour line labels
  res@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels   
  res@gsnAddCyclic = False
  res@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
  res@gsnContourZeroLineThicknessF = 0. 




; for contours  
  res1 = True
  res1@cnFillOn             = False               ; turn on color for contours
  res1@cnLinesOn            = True              ; turn off contour lines
  res1@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
  res1@gsnDraw             = False           ; don't draw
  res1@gsnFrame            = False           ; don't advance frame
  res1@cnInfoLabelOn       = False           ; turn off cn info label
  res1@gsnAddCyclic = False
  res1@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
  res1@gsnContourZeroLineThicknessF = 0. 
  res1@cnLineLabelsOn       = True             ; turn off contour line labels

  res@gsnLeftStringFontHeightF   = 0.02		; instead of using txFontHeightF or gsnStringFontHeightF 
  ;res@gsnCenterStringFontHeightF = 0.02			; to set the gsnLeft/Center/RightString font heights,
  res@gsnRightStringFontHeightF  = 0.02		; individually set each string's font height.


  ntimes = 6
  plot = new(ntimes,graphic)
  resP                     = True                ; modify the panel plot    
  resP@gsnPanelLabelBar    = True                ; add common colorbar
  resP@lbLabelFontHeightF  = 0.007               ; make labels smaller
  res@lbLabelBarOn = False
;    res@gsnMaximize  = True
  resP@gsnPanelMainString = "Column MSE Budget - Base point " + latRef+"N; "+ll+EW

  plotlabels = (/"(a)","(b)","(c)","(e)","(f)","(g)","(h)"/)
;  plotlabels = (/"(f)","(g)","(h)","(i)","(j)","(k)","(l)"/)




  
  min1 = -2.5     ; Report this scale factor in figure caption as this is only for plotting
  max1 =  2.5     ; convenience.  
  int1 =  .5
  min2 = -2.5     ; Report this scale factor in figure caption as this is only for plotting
  max2 =  2.5     ; convenience.  
  int2 =  .5


; <H, HT>----------------------------------------------------------------------  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "MSE (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,HPC, res)



  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(0) = plotA

;  draw(plotA)
;  frame(wks)
  

; <HT,advWHPC>----------------------------------------------------------------------
  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Vert Advec. (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,advWHPC, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(2) = plotA


;  draw(plotA)
;  frame(wks)


  

; <HT,advWHPC>----------------------------------------------------------------------
  
  
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  
  res@gsnLeftString = "Horz. Advec. (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,advHPC, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(1) = plotA


; <H, HT>----------------------------------------------------------------------
  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Net Radiative Flux (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."
  plotA = gsn_csm_contour_map_ce(wks,QNet, res)



  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(3) = plotA

;  draw(plotA)
;  frame(wks)


  

; <HT,SSHF>----------------------------------------------------------------------
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Net Sfc. Heat Flux (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,SFLUX, res)

  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(4) = plotA


; <HT,resid>----------------------------------------------------------------------
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Residual (shaded), MSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,resid, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,HPCT, res1)
  overlay(plotA,plotB)
  plot(5) = plotA

  gsn_panel(wks,plot,(/3,2/),resP) 
;  frame(wks)


;======================================================================================
;======================================================================================  


;DSE  


  colorMap  = "BlueDarkRed18"

  wks = gsn_open_wks("pdf" ,outPutFile2)                      ; ps,pdf,x11,ncgm,eps
  gsn_define_colormap(wks,colorMap)
  
;----------------------------------------------------------------------  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "DSE (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,DPC, res)

  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(0) = plotA

;----------------------------------------------------------------------
  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Vert Advec. (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,advWDPC, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(2) = plotA

;----------------------------------------------------------------------
  
  
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  
  res@gsnLeftString = "Horz. Advec. (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,advDPC, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(1) = plotA


; <H, HT>----------------------------------------------------------------------
  
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Net Radiative Flux (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."
  plotA = gsn_csm_contour_map_ce(wks,QNet, res)



  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(3) = plotA

;----------------------------------------------------------------------
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Net Sfc. Heat Flux (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,SFLUX, res)

  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(4) = plotA


;----------------------------------------------------------------------
  ic = 0
  res@cnMinLevelValF       = min1               ; set min contour level
  res@cnMaxLevelValF       = max1                 ; set max contour level
  res@cnLevelSpacingF      = int1               ; set contour spacing  
  res@gsnLeftString = "Residual (shaded), DSE Tendency (contours)" 
  res@gsnRightString =  "  Lag hour = 0."    
  plotA = gsn_csm_contour_map_ce(wks,resid, res)


  res1@cnMinLevelValF       = min2               ; set min contour level
  res1@cnMaxLevelValF       = max2                 ; set max contour level
  res1@cnLevelSpacingF      = int2               ; set contour spacing
  plotB = gsn_csm_contour(wks,DPCT, res1)
  overlay(plotA,plotB)
  plot(5) = plotA


  resP@gsnPanelMainString = "Column DSE Budget - Base point " + latRef+"N; "+ll+EW
  gsn_panel(wks,plot,(/3,2/),resP) 

  
   end
