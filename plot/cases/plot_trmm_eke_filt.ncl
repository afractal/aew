; PROGRAM:
; Aiyyer
; Part of the AEW diagnostic package
;
;
; Plots filtered data
; This program will plot unfiltered trmm and 2-10 day eke for three levels
;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin


  level2 = 925
  level1 = 850
  level0 = 600

  eraDir = "/typhoon/era/"
  trmmFile = "/data3/trmm/trmm3b42.anom.nc"

;  title = "V, Vorticity and TRMM "
  title = "TRMM, 2-10 EKE "

  do year = 2006,2006
    
    
    month = 7
    day   = 1
    hour  = 0
    ntimes = 500


; helene
    month = 8
    day   = 28
    hour  = 0
    ntimes = 80


    units = "hours since 1900-01-01 00:00:00"
    date = dble2flt(cd_inv_calendar(year,month,day,hour,0,0,units, 0))
    print (date)
    
    
; in current form lat lon data for TC tracks is cumbersome to use. This is an inelegant
; but effective hack to get lat-lons for specific dates
; 
    iBFil = "/typhoon/ibtracs/Allstorms.ibtracs_wmo.v03r06.nc"
    iBf     = addfile(iBFil, "r")
    iBTimeUnits = iBf->time_wmo@units
    ibLats = ndtooned (short2flt(iBf->lat_wmo))
    ibLons = ndtooned (short2flt(iBf->lon_wmo))
    Filedates   = ndtooned (doubletofloat(iBf->time_wmo) )
    Filedates@units = iBTimeUnits
    ibDate = cd_convert(date,iBTimeUnits)
    base   = min ( ind( Filedates .ge. ibDate  ) )
;
    Filedates2 = Filedates (base:) 
    ibLats2   = ibLats (base:) 
    ibLons2   = ibLons (base:) 
;
    delete(ibLats)
    delete(ibLons)
    delete(Filedates)
    
    

; read the filtered winds

 
    
;----------------------------------------------------------------------------------------------
    
    ncgmfile = "aew_trmm" + year
    
    wks = gsn_open_wks("ncgm" , ncgmfile)                      ; ps,pdf,x11,ncgm,eps
                                ; gsn_define_colormap(wks,"GMT_polar")
                                ; gsn_define_colormap(wks,"hotcolr_19lev")
    colors = (/"white","black", "white"    \
    ,"PaleTurquoise","PaleGreen","SeaGreen3" ,"Yellow"  \
    ,"Orange","HotPink","Red","Violet", "Purple", "Brown"/)
    
    gsn_define_colormap(wks, colors)               ; generate new color map
    
    
    
    res                     = True
    
    
    res@gsnDraw             = False           ; don't draw
    res@gsnFrame            = False           ; don't advance frame
    res@cnInfoLabelOn       = False           ; turn off cn info label
    
    res@mpMinLonF          = -100           ; choose a subregion
    res@mpMaxLonF          =  60.
    res@mpMinLatF          = -10           ; choose a subregion
    res@mpMaxLatF          =  40.
    res@pmTickMarkDisplayMode = "Always"
    res@mpFillOn              =  False          ; turn off map fill
    res@mpOutlineDrawOrder    = "PostDraw"      ; draw continental outline last

    
    res@mpPerimOn              = True
    res@mpFillOn               = False
    res@mpLabelsOn             = False
    res@lbLabelBarOn = False

    
    res@cnFillOn             = True     ; turn on color fill
    res@cnLinesOn            = False    ; turn of contour lines
                                ;res@cnFillMode           = "CellFill"           ; Cell Mode
    res@cnFillMode           = "RasterFill"         ; Raster Mode
    res@cnLineLabelsOn       =  False       ; Turn off contour lines
    res@cnLevelSelectionMode = "ExplicitLevels"              
    res@cnMissingValFillPattern = 0
    res@cnMissingValFillColor   = "black"
    
    res@gsnAddCyclic = True


    res@cnLevels             = (/0.1,0.3,0.5,1,2,3,5,10,15,20/) ; "mm/3hr" 
    txres               = True   
    txres@txFontHeightF = .03             ; Set the font height

   
;----------------------------------------------------------------------------------------
    resV = True
    resV@gsnDraw             = False           ; don't draw
    resV@gsnFrame            = False           ; don't advance frame
    resV@cnInfoLabelOn       = False           ; turn off cn info label
    
    resV@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
    resV@cnMinLevelValF       = -20.0               ; set min contour level
    resV@cnMaxLevelValF       =  20.                 ; set max contour level
    resV@cnLevelSpacingF      =  2.0               ; set contour spacing
    resV@cnFillOn             = False               ; turn on color for contours
    resV@cnLinesOn            = True              ; turn off contour lines
    resV@cnLineLabelsOn       = False              ; turn off contour line labels
    resV@cnLineThicknessF= 1. 
    resV@gsnAddCyclic = True
    resV@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
    resV@gsnContourZeroLineThicknessF = 0.        ; sets thickness of zero contour to 3.5 (default=1)

;---------------------------------------------------------------------------------------


    resE = True
    resE@gsnDraw             = False           ; don't draw
    resE@gsnFrame            = False           ; don't advance frame
    resE@cnInfoLabelOn       = False           ; turn off cn info label
    
    resE@cnLevelSelectionMode = "ManualLevels"     ; set manual contour levels
    resE@cnMinLevelValF       =  5.0               ; set min contour level
    resE@cnMaxLevelValF       =  200.                 ; set max contour level
    resE@cnLevelSpacingF      =  5.0               ; set contour spacing
    resE@cnFillOn             = False               ; turn on color for contours
    resE@cnLinesOn            = True              ; turn off contour lines
    resE@cnLineLabelsOn       = False              ; turn off contour line labels
    resE@cnLineThicknessF= 1. 
    resE@gsnAddCyclic = True
    resE@gsnContourNegLineDashPattern = 1       ; sets negative contours to dash pattern 1
    resE@gsnContourZeroLineThicknessF = 0.        ; sets thickness of zero contour to 3.5 (default=1)


;------------------------------------------------------------------------------------------------------



    resP                     = True                ; modify the panel plot    
    resP@gsnPanelLabelBar    = True                ; add common colorbar
    resP@lbLabelFontHeightF  = 0.007               ; make labels smaller

;---------------------------------------------------------------------------------------
                         


; ---------------Main loop over all times 
    


    filtU = "/typhoon/projects/data/filtered/U2T10Day"+year+".nc"
    filtV = "/typhoon/projects/data/filtered/V2T10Day"+year+".nc"

    fU     = addfile(filtU, "r")
    fV     = addfile(filtV, "r")


    
    do itimes = 0,ntimes
      
      date0 = cd_calendar( date,-2)
      dateArray = cd_calendar( date,0)
      hh = "" + dateArray(0,3)
      if ( dateArray(0,3) .lt. 10 ) then
        hh = "0" + dateArray(0,3)
      end if
      trmmFil = "/typhoon/trmm/3B42." + date0 + "." +  hh + ".7A.nc"
      ftr     = addfile(trmmFil, "r")
      pcp = ftr->pcp
      


      date2 = cd_calendar( date,-3)
      print (itimes + " " + date0 + hh + " " + date2 )
      


; read the winds
      u1 = fU->UBP({date2},{level0},:,:)


      v1 = fV->VBP({date2},{level0},:,:)

      u2 = fU->UBP({date2},{level1},:,:)
      v2 = fV->VBP({date2},{level1},:,:)

 
      u3 = fU->UBP({date2},{level2},:,:)
      v3 = fV->VBP({date2},{level2},:,:)

      u1&lat@units = "degrees north"
      u1&lon@units = "degrees east"
      v1&lat@units = "degrees north"
      v1&lon@units = "degrees east"
      u2&lat@units = "degrees north"
      u2&lon@units = "degrees east"
      v2&lat@units = "degrees north"
      v2&lon@units = "degrees east"
      u3&lat@units = "degrees north"
      u3&lon@units = "degrees east"
      v3&lat@units = "degrees north"
      v3&lon@units = "degrees east"


;----------------------vorticity--------------------------
;      vr1 = u1
;      vr1 = uv2vr_cfd(u1,v1,u1&lat,u1&lon, 2) 
;      vr1 = vr1*1.e5
;      vr2 = u2
;      vr2 = uv2vr_cfd(u2,v2,u2&lat,u2&lon, 2) 
;      vr2 = vr2*1.e5
;      
;      vr3 = u3
;      vr3 = uv2vr_cfd(u3,v3,u3&lat,u3&lon, 2) 
;      vr3 = vr3*1.e5
;----------------------vorticity--------------------------

;----------------------eke-------------------------------


      eke1 = u1
      eke1 = .5*(u1*u1 + v1*v1)
      eke2 = u2
      eke2 = .5*(u2*u2 + v2*v2)
      eke3 = u2
      eke3 = .5*(u3*u3 + v3*v3)


 

      res@tiMainString         = " "
      res@gsnRightString = " "
      resE@gsnLeftString  = " "
      resE@gsnRightString = " "
      
;
      plot = new(3, "graphic")
;
      res@gsnLeftString  = level0 + " hPa"
      plot(0)   = gsn_csm_contour_map(wks,pcp(0,:,:),res)
      ;plotAdd  = gsn_csm_contour(wks,v1,resV)
      plotAdd  = gsn_csm_contour(wks,eke1,resE)

      overlay(plot(0),plotAdd)
      
    
;
      res@gsnLeftString  = level1 + " hPa"
      plot(1)  = gsn_csm_contour_map(wks,pcp(0,:,:),res)
      ;plotAdd  = gsn_csm_contour(wks,v2,resV)
      plotAdd  = gsn_csm_contour(wks,eke2,resE)

      overlay(plot(1),plotAdd)
;
      res@gsnLeftString  = level2 + " hPa"
      plot(2)  = gsn_csm_contour_map(wks,pcp(0,:,:),res)
      ;plotAdd  = gsn_csm_contour(wks,v3,resV)
      plotAdd  = gsn_csm_contour(wks,eke3,resE)

      overlay(plot(2),plotAdd)
;
; now plot the location of the TC
; convert era date to ibtracs date
      ibDate = cd_convert(date,iBTimeUnits)
      print (date)
      print (ibDate)
      TCIndices  = ind( Filedates2 .eq. ibDate )
      
; TCIndices should have no missing values. If it has, then
; it is because no stroms present on this time
      
      if(any(ismissing(TCIndices))) then
                                ; SKIP
      else
        dims = dimsizes(TCIndices)
        ns = dims(0)    
        do j = 0,ns-1
          is = TCIndices(j)
          print ( itimes + " " + ibLons2(is) + " " + ibLats2(is) ) 
          hurri = gsn_add_text(wks,plot(0), "~F37~p~F",ibLons2(is),ibLats2(is), txres ) 
          hurri = gsn_add_text(wks,plot(1), "~F37~p~F",ibLons2(is),ibLats2(is), txres ) 
        end do
      end if
      
      resP@gsnPanelFigureStrings= (/"a","b"/) ; add strings to panel
      resP@amJust   = "TopLeft"
      resP@txString   = title + date2
      gsn_panel(wks,plot,(/3,1/),resP)               ; now draw as one plot
      
      
    ;draw(plot)
    ;frame(wks)
    delete(TCIndices)
    
    date = date + 6
    delete (u1)
    delete (v1)

    delete (u2)
    delete (v2)

    delete (u3)
    delete (v3)


  end do
  
  delete ( Filedates2 )
  delete ( ibLats2)
  delete ( ibLons2)
end do


end

