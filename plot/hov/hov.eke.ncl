; PROGRAM:
; Aiyyer
; Part of the AEW diagnostic package
;
; details
;
;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"  
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"

begin


  

  year = 2006
  date1 = 2006090100
  date2 = 2006091618

 
  hvb = 5.
  hvt = 15.

  YlabInt = 4
  Title = "2-10 Day Filtered EKE"


  level = 925


  ncgmFile = "Helene2-10EKE." + level
  Title =  "Helene 2-10 EKE " + level



  eraDir = "/typhoon/era/"
  trmmFile = "/data3/trmm/trmm3b42.anom.nc"
  trmmSuffix  = ".7A.nc"

  era_filt_V_file = "/typhoon/projects/aew/filter/data/V2T10Day"+year+".nc"
  era_filt_U_file = "/typhoon/projects/aew/filter/data/U2T10Day"+year+".nc"

  fFiltV     = addfile(era_filt_V_file, "r")
  fFiltU     = addfile(era_filt_U_file, "r")

  vFilt = fFiltV->VBP({date1:date2},{level},:,:)
  uFilt = fFiltU->UBP({date1:date2},{level},:,:)

  printVarSummary(uFilt)
  printVarSummary(vFilt)

  eke = uFilt
  eke = .5*(uFilt*uFilt +  vFilt*vFilt)

  delete (vFilt)
  delete (uFilt)

  Hov = dim_avg_n_Wrap(eke(:,{hvb:hvt},:),1)
  HovFlip = lonFlip (Hov)
  delete(Hov)



;--------------------------------------------------------------------------------------------
; convert the dates to useful form for writing the time axis
;
  time = HovFlip&time
  yearA  = time/1000000
  monthA = (time - yearA*1000000)/10000
  dayA   = (time - yearA*1000000 - monthA*10000)/100
  hourA  = (time - yearA*1000000 - monthA*10000 - dayA*100)
  minuteA = hourA*0
  secA    = hourA*0
  units  = "hours since 1900-01-01 00:00:00" ; "seconds/hours/days since ...."
  timeA   = cd_inv_calendar(yearA,monthA,dayA,hourA,minuteA,secA,units, 0)
  timeA!0 = "time"
  Hov = (/HovFlip/)
  Hov!0 = "Time" ; Name the time dimension but do not set the Time values.  
  Hov!1 = "lon"  ; name the x dimension
  Hov&lon = HovFlip&lon ; set the x dimension values

;--------------------------------------------------------------------------------------------
; Open workstation
  wks_type = "ncgm"
  wks_type@wkWidth = 3600
  wks_type@wkHeight = 3600
  wks  = gsn_open_wks(wks_type,ncgmFile)

  ;---Using the color map to define a color map for the workstation
  gsn_define_colormap(wks,"sunshine_9lev")

; Modifications to plot
  resV                              = True         
  resV@gsnDraw                      = False             ; Do not draw the plot
  resV@gsnFrame                     = False             ; Do not advance the frame
  resV@cnFillOn                     = True             ; Turn on color fill
  resV@cnLinesOn                    = False             ; Turn off contour lines                               
  resV@cnLevelSelectionMode         = "ManualLevels"    ; Set manual contour levels
  resV@cnMinLevelValF               =  2.         ; Minimum level
  resV@cnMaxLevelValF               =  62.         ; Maximum level
  resV@cnLevelSpacingF              =  6.          ; Contour spacing
  resV@cnLineLabelsOn       = False   
  resV@gsnMajorLonSpacing           = 20.                ; Major horizontal tick int
  resV@gsnMinorLonSpacing           = 10.                ; Minor horizontal tick int
  resV@cnInfoLabelOn       = False 

  resV@gsnMaximize                  = True              ; Maximize plot in frame
  resV@gsnContourZeroLineThicknessF = 0.   
  resV@gsnContourNegLineDashPattern = 1                 ; sets negative contours to dash pattern 1


  resV@tmYLMode      = "Explicit"                ; Define own tick mark labels.
  dims = dimsizes(timeA)
  nt = dims(0)
  nl = nt/YlabInt  + 1
  print (nt + " " + nl )


  YLabs = new((/nl/),"string")      
  YLVal = new((/nl/),"float") 

  format = "%D %c" ; formay the y-label string. see doc on cd_string
  ii = 0 
  do i = 0,nt-1,YlabInt
    YLVal(ii) = i
    YLabs(ii) =cd_string( timeA(i),format)
    ii = ii + 1
  end do
  
  resV@tmYLValues = YLVal
  resV@tmYLLabels = YLabs

;--------------------------------------------------------------------------------------------
  resV@tiMainFont                    = "times-roman"    ; set font of title
  resV@tiMainFontAspectF                 = 1.0              ; title aspect ratio
  resV@tiMainString                  = Title              ; Title
  resV@tiMainFontHeightF             = 0.020            ; title font height
  resV@tiMainFontThicknessF          = 1.0             ; title font thickness
  resV@tiMainOffsetXF                = 0.00             ; title font horiz. offset

  ;~~ Axes labels modifications ~~;
  resV@tmYLLabelFont                 = 25               ; times font, roman
  resV@tmYLLabelFontAspectF          = 1.15             ; times aspect ratio
  resV@tmYLLabelFontHeightF          = 0.02             ; times font height
  resV@tmYLLabelFontThicknessF       = 1.0              ; times font thickness
  resV@tmXBLabelFont                 = 25               ; lon font, roman 
  resV@tmXBLabelFontAspectF          = 1.15             ; lon aspect ratio
  resV@tmXBLabelFontHeightF          = 0.02             ; lon font height
  resV@tmXBLabelFontThicknessF       = 0.5              ; lon font thickness


  resV@lbOrientation                = "Vertical"  

  resV@vpWidthF                     = .6             ; Set width of plot
  resV@vpHeightF                    = .8            ; Set height of plot;

  printVarSummary(Hov)

  plot = gsn_csm_hov(wks,Hov(:,{-60:60}),resV)
  draw(plot)


end

